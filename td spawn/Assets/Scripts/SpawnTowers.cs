﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTowers : MonoBehaviour
{
    public GameObject spawnPrefab, towerPrefab;
    public GameObject[] spawnPlaces;
    public bool[] placesReserved;
    bool canSpawnObject = true;
    int currentNumberOftowers = 0;
    // Use this for initialization
    void Start()
    {
        placesReserved = new bool[spawnPlaces.Length];
        for (int i = 0; i < placesReserved.Length; i++)
        {
            placesReserved[i] = false;
        }
        StartCoroutine(SpawnTowersInTime());
    }
    IEnumerator SpawnTowersInTime()
    {
        SpawnTower();
        while (currentNumberOftowers < 20)
        {
            yield return new WaitForSeconds(15);
            SpawnTower();
        }
        while (true)
        {
            yield return new WaitForSeconds(120);
            SpawnTower();
        }
    }

    public void SpawnTower()
    {
       
        if (currentNumberOftowers == 20) return;
        if (canSpawnObject)
        {
            canSpawnObject = false;
            int index = Random.Range(0, placesReserved.Length);
            while (placesReserved[index])
                index = Random.Range(0, placesReserved.Length);
            placesReserved[index] = true;
            Destroy(spawnPlaces[index], .1f);
            spawnPlaces[index] = (GameObject)Instantiate(towerPrefab, spawnPlaces[index].transform.position, spawnPlaces[index].transform.rotation);
            currentNumberOftowers++;
        }
        canSpawnObject = true;
    }
}
