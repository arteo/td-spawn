﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    bool bulletNotExist = true;
    public Transform barrelGun;
    public GameObject bulletPrefab;
    public GameObject spawnPlanePrefab;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(RoteteAndShoot());
    }

    IEnumerator RoteteAndShoot()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(5f, 10f));
            bulletNotExist = false;
            var bullet = (GameObject)Instantiate(bulletPrefab, barrelGun.position, barrelGun.rotation);

            // Add velocity to the bullet
            bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 60;

            // Destroy the bullet after 2 seconds
            Destroy(bullet, 9.9f);

            transform.Rotate(0, Random.Range(15f, 60f), 0);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        
        if (collision.gameObject.tag == "bullet")
        {
            Debug.Log(transform.position);
            Instantiate(spawnPlanePrefab, transform.position,Quaternion.identity);
            Destroy(gameObject);
        }
    }

}
